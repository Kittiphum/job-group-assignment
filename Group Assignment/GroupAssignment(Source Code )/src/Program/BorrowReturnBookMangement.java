package Program;

import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.Month;

import Model.Book;
import Model.BookUser;
import Model.BorrowReturnBook;
import service.serviceUser;

public class BorrowReturnBookMangement {
	
	private serviceUser serviceuser = new serviceUser();
	
	private BookMangement bookMangement = new BookMangement();
	
	public ArrayList<BorrowReturnBook> addnBorrow(ArrayList<BorrowReturnBook> BorrowReturnBookList ,ArrayList<Book> BookList , ArrayList<BookUser> User, int[] UserAndBook , int Amount) {
		Scanner sc = new Scanner(System.in);
		BorrowReturnBook borrow_retur = new BorrowReturnBook();
		int UserId = UserAndBook[0];
		int BookId = UserAndBook[1];
		
		if(BorrowReturnBookList.size() == 0) {
			borrow_retur.setCodeBorrowed(CreateUserId(2));
		}else { 
			borrow_retur.setCodeBorrowed(CreateUserId(Integer.parseInt(BorrowReturnBookList.get(BorrowReturnBookList.size()-1).getCodeBorrowed()) +2));
		}
		System.out.println("\n<<<BorrowedDate>>>" );
		LocalDate BorrowedDate = LocalDate.now();
		System.out.println("\nBorrowedDate : " +  BorrowedDate.toString());
		borrow_retur.setBorrowedDate(BorrowedDate.toString());
		
		System.out.println("\n<<<WantDateReturn>>>" );
		String ReturnDate = InputDate();
		System.out.println("\nReturnDate : " +  ReturnDate);
		borrow_retur.setWantDateReturn(ReturnDate);
		
		borrow_retur.setReturnDate("Not yet returned");
		
		borrow_retur.setUserId(User.get(UserId).GetUserId());
		
		borrow_retur.setProductId(BookList.get(BookId).getId());
		
		borrow_retur.setAmountBook(Amount);
		
		BorrowReturnBookList.add(borrow_retur);
		System.out.println("\n                               <<<Borrow Detail>>>");
		displayIF(BorrowReturnBookList  , borrow_retur.getCodeBorrowed());
		System.out.println("\n<<<Borrow Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		
		return BorrowReturnBookList;
	}
	
	public int[] ReturnBook(ArrayList<BorrowReturnBook> BorrowReturnBookList ,ArrayList<Book> BookList) {
		
		Scanner sc = new Scanner(System.in);
		boolean ReturnComplete = false;
		boolean HasIdBorrow = false;
		int IdBorrow = 0;
		int IdBook = 0;
		
		int IdBorrowAndBookList[] = new int[2];
		
		System.out.println("\n                               <<Borrowed List>>" );
		display(BorrowReturnBookList);
		
		while (ReturnComplete==false) {
			System.out.print("\nInput CodeBorrowed :" );
			String CodeBorrowed = sc.next();
			
			for(int i =0 ; i<BorrowReturnBookList.size() ; i++) {
				if(BorrowReturnBookList.get(i).getCodeBorrowed().equals(CodeBorrowed)) {
					IdBorrow = i;
					HasIdBorrow = true;
					break;
				}
			}
			
			if(HasIdBorrow == true) {
				IdBorrowAndBookList[0] = IdBorrow;
				for(int i =0 ; i<BookList.size() ; i++) {
					if(BookList.get(i).getId().equals(BorrowReturnBookList.get(IdBorrow).getProductId())) {
						IdBook = i;
						break;
					}
				}
				IdBorrowAndBookList[1] = IdBook;
				ReturnComplete = true;
			}else {
				System.out.println("\nNo have CodeBorrowed Input again" );
			}
		}
		
		return IdBorrowAndBookList;
	}
	
	public ArrayList<Book> UpdateBorrowReturnBook(ArrayList<Book> BookList , ArrayList<BorrowReturnBook> BorrowReturnBookList, int[] IdBorrowAndBookList) {
		int IdBorrow = IdBorrowAndBookList[0];
		int IdBook = IdBorrowAndBookList[1];
		
		BookList.get(IdBook).setAmountNow( BookList.get(IdBook).getAmountNow() + BorrowReturnBookList.get(IdBorrow).getAmountBook());
		BookList.get(IdBook).setAmountBorrow( BookList.get(IdBook).getAmountBorrow() - BorrowReturnBookList.get(IdBorrow).getAmountBook());
		
		return BookList;
	}
	
	public ArrayList<BorrowReturnBook> UpdateReturnBook(ArrayList<BorrowReturnBook> BorrowReturnBookList , int[] IdBorrowAndBookList) {
		int IdBorrow = IdBorrowAndBookList[0];
		System.out.print("\n<<Input Return Date>>" );
		String ReturnDate = InputDate();
		BorrowReturnBookList.get(IdBorrow).setReturnDate(ReturnDate);
		
		return BorrowReturnBookList;
	}
	
	public void displayIF(ArrayList<BorrowReturnBook> BorrowReturnBookList , String BorrowID) {
		String interval1 = "            ";
		String interval2 = "           ";
		String interval3 = "           ";
		String interval4 = "         ";
		String interval5 = "       ";
		String interval6 = "";//14
		System.out.println("CodeBorrowed       BorrowedDate        WantDateReturn     BorrowerID     BookID       AmountBook      ReturnDate");
		for(int i =0 ;i<BorrowReturnBookList.size();i++) {
			if(BorrowReturnBookList.get(i).getCodeBorrowed().equals(BorrowID)) {
				interval6 = serviceuser.ManageInterval(interval6 , String.valueOf(BorrowReturnBookList.get(i).getAmountBook()), 13);
				System.out.println("    "+BorrowReturnBookList.get(i).getCodeBorrowed() + interval1 +
									BorrowReturnBookList.get(i).getBorrowedDate() + interval2 +
									BorrowReturnBookList.get(i).getWantDateReturn()+ interval3+
									BorrowReturnBookList.get(i).getUserId() + interval4+
									BorrowReturnBookList.get(i).getProductId() +interval5 +
									BorrowReturnBookList.get(i).getAmountBook()+ interval6 +
									BorrowReturnBookList.get(i).getReturnDate()
									);
			}
		}
		
	}
	
	public void display(ArrayList<BorrowReturnBook> BorrowReturnBookList) {
		String interval1 = "            ";
		String interval2 = "           ";
		String interval3 = "          ";
		String interval4 = "         ";
		String interval5 = "           ";
		String interval6 = "";//14
		System.out.println("CodeBorrowed       BorrowedDate        WantDateReturn     BorrowerID     BookID       AmountBook      ReturnDate");
		for(int i =0 ;i<BorrowReturnBookList.size();i++) {
			interval6 = serviceuser.ManageInterval(interval6 , String.valueOf(BorrowReturnBookList.get(i).getAmountBook()), 13);
			System.out.println("    "+BorrowReturnBookList.get(i).getCodeBorrowed() + interval1 +
								BorrowReturnBookList.get(i).getBorrowedDate() + interval2 +
								BorrowReturnBookList.get(i).getWantDateReturn()+ interval3+
								BorrowReturnBookList.get(i).getUserId() + interval4+
								BorrowReturnBookList.get(i).getProductId() +interval5 +
								BorrowReturnBookList.get(i).getAmountBook()+ interval6 +
								BorrowReturnBookList.get(i).getReturnDate()
								);
		}
		
	}
	
	private static String CreateUserId(int numUser) {
		int lengthID = 5;
		String ID = "";
		for(int i =0 ; i<numUser ;i++) {
			ID = String.valueOf(i);
			if(ID.length()<lengthID) {
				for(int j = 0 ; j<=lengthID-ID.length();j++) {
					ID = "0"+ID ;
				}
			}
		}
		
		return ID;
	}
	
	private String InputDate() {
		Scanner sc = new Scanner(System.in);
		
		int month = 0 ;
		String monthString = "" ;
		int day = 0;
		String dayString = "";
		String year = "" ;
		
		boolean InputYearhComplete = false;
		boolean InputMonthComplete = false;
		boolean InputDayComplete = false;
		
		
		while (InputYearhComplete == false) {
			System.out.print("\nInput year : " );
			year = sc.next();
			if(year.length() > 4) {
				System.out.print("\nYear is not correct input Again" );
			}else {
				InputYearhComplete = true;
			}
		}
		
		while (InputMonthComplete == false) {
			System.out.print("\nInput month : " );
			month = sc.nextInt();
			if(month > 12 || month <= 0) {
				System.out.print("\nMonth is not correct input Again" );
			}else {
				InputMonthComplete = true;
			}
		}
		if(month<10) {
			monthString = "0"+month;
		}else {
			monthString = String.valueOf(month);
		}
		
		while (InputDayComplete == false) {
			System.out.print("\nInput day : " );
			day = sc.nextInt();
			if(month == 1 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 2 && day > 0 && day <= 29) {
				InputDayComplete = true;
			}
			else if(month == 3 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 4 && day > 0 && day <= 30) {
				InputDayComplete = true;
			}
			else if(month == 5 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 6 && day > 0 && day <= 30) {
				InputDayComplete = true;
			}
			else if(month == 7 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 8 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 9 && day > 0 && day <= 30) {
				InputDayComplete = true;
			}
			else if(month == 10 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}
			else if(month == 11 && day > 0 && day <= 30) {
				InputDayComplete = true;
			}
			else if(month == 12 && day > 0 && day <= 31) {
				InputDayComplete = true;
			}else {
				System.out.print("\nDay is not correct input Again" );
			}
		}
		if(day<10) {
			dayString = "0"+day;
		}else {
			dayString = String.valueOf(day);
		}
		
		return year+"-"+monthString+"-"+dayString;
	}
	
	public int[] NameUserandBookBorrow(ArrayList<Book> BookList , ArrayList<BookUser> User) {
		int UserAndBook [] = new int[2];
		Scanner sc = new Scanner(System.in);
		ArrayList<String> IdHave = new ArrayList<String>();
		
		System.out.print("Input Name borrower : " );
		String NameUser = sc.next();
		
		int IdUser = ChackNameUser(NameUser , User);
		
		System.out.println("Name borrower : " + User.get(IdUser).GetUserName());
		System.out.print("Input Name Book you want Borrow : " );
		String NameBook = sc.next();
		
		int IdBook = ChackNameBook(NameBook , BookList);
		
		UserAndBook[0] = IdUser;
		UserAndBook[1] = IdBook;
		
		return UserAndBook;
	}
	
	public int UpdateBookList(ArrayList<Book> BookList , int[] UserAndBook) {
		Scanner sc = new Scanner(System.in);
		
		boolean BorrowComplete = false;
		int AmountBorrow = 0;
		
		int IdUser = UserAndBook[0];
		int IdBook = UserAndBook[1];
		
		
		while(BorrowComplete == false) {
		
			if(BookList.get(IdBook).getAmountNow() > 0) {
				System.out.println("\nCan Borrow");
				System.out.print("Input Amount Borrow : ");
				AmountBorrow = sc.nextInt();
				if(BookList.get(IdBook).getAmountNow() - AmountBorrow >= 0) {
//					BookList.get(IdBook).setAmountNow(BookList.get(IdBook).getAmountNow() - AmountBorrow);
//					BookList.get(IdBook).setAmountBorrow(BookList.get(IdBook).getAmountBorrow() + AmountBorrow);
					BorrowComplete = true;
					break;
				}else {
					System.out.println("\nThere are not enough numbers as needed Input again");
				}
			}
			else {
				System.out.println("\nCan Not Borrow because book is no more");
				BorrowComplete = true;
				break;
			}
			
		}
		
		
		
		return AmountBorrow;
	}
	
	
	public ArrayList<Book> UpdateBookList(ArrayList<Book> BookList , int[] UserAndBook , int Amount) {
		Scanner sc = new Scanner(System.in);
		
		boolean BorrowComplete = false;
		int AmountBorrow = Amount;
		
		int IdBook = UserAndBook[1];
		
		BookList.get(IdBook).setAmountNow(BookList.get(IdBook).getAmountNow() - AmountBorrow);
		BookList.get(IdBook).setAmountBorrow(BookList.get(IdBook).getAmountBorrow() + AmountBorrow);
		
		return BookList;
	}
	
	private int ChackNameBook(String Name ,  ArrayList<Book> BookList) {
		boolean InputComplete = false;
		int NumHasBook = 0;
		int idexBook = 0 ;
		int IdBorrower = 0;
		boolean HasBook = false;
		ArrayList<String> IdHave = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		
		while(InputComplete == false) {
			for(int i = 0 ; i < BookList.size() ; i++) {
				if(BookList.get(i).getName().equals(Name)) {
					NumHasBook++;
					idexBook = i;
					HasBook = true;
					IdHave.add(BookList.get(i).getId());
				}
			}
			if(NumHasBook > 1) {
				System.out.println("<<Have more names>>");
				bookMangement.displayIf(BookList , Name);
				BorrowReturnBook borrowreturnbook = new BorrowReturnBook();
				while (InputComplete == false) {
					System.out.print("\nInput Id want io borrower : ");
					String inputIdBorrower = sc.next();
					for(int i =0 ;i<IdHave.size();i++) {
						if(IdHave.get(i).equals(inputIdBorrower)) {
							IdBorrower = idexBook;
							InputComplete = true;
						}
					}
					
					if(InputComplete == true) {
						break;
					}
					else {
						System.out.print("\nNo Have ID Input Again");
					}
				}
				
			}
			else if(NumHasBook == 1) {
				System.out.println("\n<<Have names>>");
				bookMangement.displayIf(BookList , Name);
				BorrowReturnBook borrowreturnbook = new BorrowReturnBook();
				IdBorrower = idexBook;
				
				InputComplete = true;
			}
			if(HasBook == false) {
				bookMangement.display(BookList);
				System.out.println("\n<<No have this Name Select Name Again>>");
				System.out.print("Input Name : ");
				Name = sc.next();
			}	
		}
		return IdBorrower;
	}
	
	
	private int ChackNameUser(String Name ,  ArrayList<BookUser> User) {
		boolean InputComplete = false;
		int NumHasUser = 0;
		int idexUser = 0 ;
		int IdBorrower = 0;
		boolean HasUser = false;
		ArrayList<String> IdHave = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		
		while(InputComplete == false) {
			for(int i = 0 ; i < User.size() ; i++) {
				if(User.get(i).GetUserName().equals(Name)) {
					NumHasUser++;
					idexUser = i;
					HasUser = true;
					IdHave.add(User.get(i).GetUserId());
				}
			}
			if(NumHasUser > 1) {
				System.out.println("<<Have more names>>");
				serviceuser.displayIF(User , Name);
				BorrowReturnBook borrowreturnbook = new BorrowReturnBook();
				while (InputComplete == false) {
					System.out.print("\nInput Id want to borrower : ");
					String inputIdBorrower = sc.next();
					for(int i =0 ;i<IdHave.size();i++) {
						if(IdHave.get(i).equals(inputIdBorrower)) {
							InputComplete = true;
						}
					}
					
					if(InputComplete == true) {
						for(int i =0 ; i< User.size() ; i++) {
							if(User.get(i).GetUserId().equalsIgnoreCase(inputIdBorrower)) {
								IdBorrower = i;
							}
						}
					}
					else {
						System.out.print("\nNo Have ID Input Again");
					}
				}
				
			}
			else if(NumHasUser == 1) {
				System.out.println("\n<<Have names>>");
				serviceuser.displayIF(User , Name);
				BorrowReturnBook borrowreturnbook = new BorrowReturnBook();
				IdBorrower = idexUser;
				InputComplete =true;
				
			}
			if(HasUser == false) {
				serviceuser.displayAll(User);
				System.out.println("\n<<No have this Name Select Name Again>>");
				System.out.print("Input Name : ");
				Name = sc.next();
			}	
		}
		return IdBorrower;
	}
	
	private void startBorrower(ArrayList<BookUser> User , BorrowReturnBook borrowreturnbook , String inputIdBorrower) {
		
	}

}
