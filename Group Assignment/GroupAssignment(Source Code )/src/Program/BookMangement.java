package Program;

import java.util.ArrayList;
import java.util.Scanner;

import Model.Book;
import service.serviceUser;

public class BookMangement {
	
	private serviceUser serviceuser = new serviceUser();
	
	
	public ArrayList<Book> AddBook(ArrayList<Book> BookList) {
		Scanner input = new Scanner(System.in);
		Book book = new Book();
		boolean HasName = false;
		boolean AddComplete ;
		int Amount = 0;
		
		System.out.println("\n            <<Add Book Start>>");
		
		System.out.print("\nInput Name Book want to add : ");
		String BookName = input.next();
		book.setName(BookName);
		for(int i =0 ; i<BookList.size();i++) {
			if(BookList.get(i).getName().equals(BookName)) {
				do{
			      try{
			  		Scanner sc = new Scanner(System.in);
			        System.out.println("                    <<Hsa Book>>");
					displayIf(BookList, BookName);
					System.out.print("\nInput Amount Book want to add (Number) : ");
					Amount = sc.nextInt();
			        AddComplete=false;
			      }
			      catch(Exception e){
			        System.out.println("\n      ****Enter only Number Input Again*****\n");
			        AddComplete=true;
			      }
			    }
			    while(AddComplete);
				int num = BookList.get(i).getAmount()+Amount;
				book.setId(BookList.get(i).getId());
				book.setAmount(num);
				book.setAmountBorrow(BookList.get(i).getAmountBorrow());
				book.setAmountNow(book.getAmount()-book.getAmountBorrow());
				BookList.set(i ,book);
				HasName = true;
				break;
			}
		}
		if(HasName == false) {
			do
		    {
		      try
		      {
		  		Scanner sc = new Scanner(System.in);
		  		book.setId(CreateUserId(Integer.parseInt(BookList.get(BookList.size()-1).getId()) +2));
				System.out.print("\nInput Amount Book want to add : ");
		        Amount=sc.nextInt();
		        AddComplete=false;
		      }
		      catch(Exception e)
		      {
		        System.out.println("Enter only Number Input Again");
		        AddComplete=true;
		      }
		    }
		    while(AddComplete);
			book.setAmount(Amount);
			book.setAmountBorrow(0);
			book.setAmountNow(book.getAmount()-book.getAmountBorrow());
			BookList.add(book);
		}
		System.out.println("\n   <<< Add Complete >>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		return BookList ;
	}
	
	public ArrayList<Book> DeleteBook(ArrayList<Book> BookList) {
		Scanner sc = new  Scanner(System.in);
		boolean DeleteComplete = false ;
		boolean HasId = false;
		
		System.out.println("\n            <<Delete Book Start>>");
		
		display(BookList);
		System.out.println("\n<<Select ID you want to delete>>");
		
		System.out.print("Input ID : ");
		String IdWantDelete = sc.next();
		
		for(int i = 0 ; i < BookList.size() ; i++) {
			if(BookList.get(i).getId().equals(IdWantDelete)) {
				BookList.remove(i);
				HasId= true;
			}
		}
		
		while (HasId == false) {
			display(BookList);
			System.out.println("\n<<No have this Id Select ID Again>>");
			System.out.print("Input ID : ");
			IdWantDelete = sc.next();
			
			for(int i = 0 ; i < BookList.size() ; i++) {
				if(BookList.get(i).getId().equals(IdWantDelete)) {
					BookList.remove(i);
					HasId= true;
				}
			}
		}
		
		System.out.println("\n<<<Delete Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		
		return BookList;
	}
	
	public ArrayList<Book> EditBook(ArrayList<Book> BookList) {
		Scanner sc = new  Scanner(System.in);
		Book book = new Book();
		boolean EditComplete = false ;
		boolean HasId = false;
		int Amount = 0 ;
		
		System.out.println("\n            <<Edit Book Start>>");
		
		display(BookList);
		System.out.println("\n<<Select ID you want to Edit>>");
		
		System.out.print("Input ID : ");
		String IdWantEdit = sc.next();
		while(EditComplete == false) {
			for(int i = 0 ; i < BookList.size() ; i++) {
				if(BookList.get(i).getId().equals(IdWantEdit)) {
					System.out.print("\nInput New Name Book : ");
					String newNameBook = sc.next();
					do{
					      try{
					  		Scanner input = new Scanner(System.in);
							System.out.print("\nInput new Amount Book want to add (Number) : ");
							Amount = sc.nextInt();
							EditComplete=false;
					      }
					      catch(Exception e){
					        System.out.println("\n      ****Enter only Number Input Again*****\n");
					        EditComplete=true;
					      }
					    }while(EditComplete);
					book.setName(newNameBook);
					book.setId(BookList.get(i).getId());
					book.setAmount(Amount);
					book.setAmountBorrow(BookList.get(i).getAmountBorrow());
					book.setAmountNow(book.getAmount()-book.getAmountBorrow());
					BookList.set(i,book);
					HasId= true;
					EditComplete = true;
				}
			}
			
			if (HasId == false) {
				display(BookList);
				System.out.println("\n<<No have this Id Select ID Again>>");
				System.out.print("Input ID : ");
				IdWantEdit = sc.next();
			}
		}
		
		System.out.println("\n<<<Edit Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		
		return BookList;
	}
	
	private static String CreateUserId(int numUser) {
		int lengthID = 5;
		String ID = "";
		for(int i =0 ; i<numUser ;i++) {
			ID = String.valueOf(i);
			if(ID.length()<lengthID) {
				for(int j = 0 ; j<=lengthID-ID.length();j++) {
					ID = "0"+ID ;
				}
			}
		}
		
		return ID;
	}
	
	public void displayIf(ArrayList<Book> BookList , String Name) {
		String interval1 = "";
		String interval2 = "";//20
		String interval3 = "";//10
		String interval4 = "";//16
		System.out.println("ID       Name               Amount     AmountBorrow     AmountNow");
		for(int i = 0 ; i<BookList.size() ; i++) {
			if(BookList.get(i).getName().equals(Name)) {
				interval2 = serviceuser.ManageInterval(interval2, BookList.get(i).getName(), 20);
				interval3 = serviceuser.ManageInterval(interval3, String.valueOf(BookList.get(i).getAmount()), 15);
				interval4 = serviceuser.ManageInterval(interval4, String.valueOf(BookList.get(i).getAmountBorrow()), 15);
				System.out.println(BookList.get(i).getId() + "     "  
									+ BookList.get(i).getName() + interval2+
									BookList.get(i).getAmount() + interval3+
									BookList.get(i).getAmountBorrow() + interval4+
									BookList.get(i).getAmountNow());
			}
		}
	}
	
	public void display(ArrayList<Book> BookList) {
		String interval1 = "";
		String interval2 = "";//20
		String interval3 = "";//10
		String interval4 = "";//16
		System.out.println("ID       Name               Amount     AmountBorrow     AmountNow");
		for(int i =0 ;i<BookList.size();i++) {
			interval2 = serviceuser.ManageInterval(interval2, BookList.get(i).getName(), 20);
			interval3 = serviceuser.ManageInterval(interval3, String.valueOf(BookList.get(i).getAmount()), 15);
			interval4 = serviceuser.ManageInterval(interval4, String.valueOf(BookList.get(i).getAmountBorrow()), 15);
			System.out.println(BookList.get(i).getId() + "     "  
								+ BookList.get(i).getName() + interval2+
								BookList.get(i).getAmount() + interval3+
								BookList.get(i).getAmountBorrow() + interval4+
								BookList.get(i).getAmountNow());
		}
		
	}
}
