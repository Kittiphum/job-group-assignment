package Program;

import java.util.ArrayList;
import java.util.Scanner;

import Model.BookUser;
import service.serviceUser;

public class UserMangement {
	
	private serviceUser serviceuser = new serviceUser();
	
	public ArrayList<BookUser> addBookUser(ArrayList<BookUser> User) {
		BookUser bookuser = new BookUser();
		if(User.size() == 0) {
			bookuser = serviceuser.startAdd(bookuser , 2);
		}else { 
			bookuser = serviceuser.startAdd(bookuser , Integer.parseInt(User.get(User.size()-1).GetUserId()) +2 );
		}
		User.add(bookuser);
		System.out.println("\n<<<Add Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		return User ;
	}
	
	public ArrayList<BookUser> deleteUser(ArrayList<BookUser> User) {
		Scanner sc = new Scanner(System.in);
		boolean HasId = false;
		
		serviceuser.displayAll(User);
		System.out.println("\n<<Select ID you want to delete>>");
		
		System.out.print("Input ID : ");
		String IdWantDelete = sc.next();
		
		for(int i = 0 ; i < User.size() ; i++) {
			if(User.get(i).GetUserId().equals(IdWantDelete)) {
				User.remove(i);
				HasId= true;
			}
		}
		
		while (HasId == false) {
			serviceuser.displayAll(User);
			System.out.println("\n<<No have this Id Select ID Again>>");
			System.out.print("Input ID : ");
			IdWantDelete = sc.next();
			
			for(int i = 0 ; i < User.size() ; i++) {
				if(User.get(i).GetUserId().equals(IdWantDelete)) {
					User.remove(i);
					HasId= true;
				}
			}
		}
		
		System.out.println("\n<<<Delete Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		
		return User;
	}
	
	public ArrayList<BookUser> editBookUser(ArrayList<BookUser> User) {
		Scanner sc = new Scanner(System.in);
		ArrayList<String> IdHave = new ArrayList<String>(); 
		boolean HasUser = false;
		boolean EditComplete = false;
		int NumHasUser = 0 ;
		int idexUser = 0;
		
		serviceuser.displayAll(User);
		
		System.out.print("\nInput Name want to Edit : ");
		String Name = sc.next();
		while(EditComplete == false) {
			for(int i = 0 ; i < User.size() ; i++) {
				if(User.get(i).GetUserName().equals(Name)) {
					NumHasUser++;
					idexUser = i;
					HasUser = true;
					IdHave.add(User.get(i).GetUserId());
				}
			}
			if(NumHasUser > 1) {
				System.out.println("<<Have more names>>");
				serviceuser.displayIF(User , Name);
				BookUser bookuser = new BookUser();
				while (EditComplete == false) {
					System.out.print("\nInput Id want io edit : ");
					String inputIdEdit = sc.next();
					for(int i =0 ;i<IdHave.size();i++) {
						if(IdHave.get(i).equals(inputIdEdit)) {
							EditComplete = true;
						}
					}
					
					if(EditComplete == true) {
						User = serviceuser.startEdit(User ,bookuser , inputIdEdit);
					}
					else {
						System.out.print("\nNo Have ID Input Again");
					}
				}
				
			}
			else if(NumHasUser == 1) {
				System.out.println("<<Have names>>");
				serviceuser.displayIF(User , Name);
				BookUser bookuser = new BookUser();
				String inputIdEdit = User.get(idexUser).GetUserId();
				
				User = serviceuser.startEdit(User ,bookuser , inputIdEdit);
				EditComplete = true;
			}
			if(HasUser == false) {
				serviceuser.displayAll(User);
				System.out.println("\n<<No have this Name Select Name Again>>");
				System.out.print("Input Name : ");
				Name = sc.next();
			}	
		}
		
		System.out.println("\n<<<Edit Complete>>>");
		System.out.println("\n<<<Press Enter to continue>>>");
		try{
			System.in.read();
			}
		catch(Exception e){
			
		}
		return User;
	}

}
