package InterFace;

public interface User {

	public String GetUserId();
	
	public String SetUserId(String UserId);
	
	public String GetUserName();
	
	public String SetUserName(String UserName);

	public String GetUserType();
	
	public String SetUserType(String UserType);

	
}