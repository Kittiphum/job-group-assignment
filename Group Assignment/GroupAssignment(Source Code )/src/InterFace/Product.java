package InterFace;

public interface Product {
	
	public String getId();
	
	public String setId(String Id);
	
	public String getName();
	
	public String setName(String Name);
	
	public int getAmount();
	
	public int setAmount(int Amount);
	
	public int getAmountNow();
	
	public int setAmountNow(int AmountNow);
	
	public int getAmountBorrow();
	
	public int setAmountBorrow(int AmountBorrow);
	
}
