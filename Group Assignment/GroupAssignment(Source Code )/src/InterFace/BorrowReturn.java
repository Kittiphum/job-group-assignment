package InterFace;

import Model.BookUser;

public interface BorrowReturn {
	
	public String getCodeBorrowed();
	
	public String setCodeBorrowed(String CodeBorrowed);
	
	public String getBorrowedDate();
	
	public String setBorrowedDate(String BorrowedDate);
	
	public String getWantDateReturn();
	
	public String setWantDateReturn(String WantDateReturn);
	
	public String getReturnDate();
	
	public String setReturnDate(String ReturnDate);
	
	public String getUserId();
	
	public String setUserId(String UserId);
	
	public String getProductId();
	
	public String setProductId(String ProductId);

}
