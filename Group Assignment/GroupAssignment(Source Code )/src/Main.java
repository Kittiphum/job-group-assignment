import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.text.StyledEditorKit.BoldAction;

import Model.Book;
import Model.BookUser;
import Model.BorrowReturnBook;
import service.ServiceCallProgram;
import service.serviceUser;

public class Main {

	public static void main(String[] args) {
		start();
	 }
	
	private static void start() {
		ArrayList<BookUser> User = new ArrayList<BookUser>();
		ArrayList<Book> BookList = new ArrayList<Book>();
		ArrayList<BorrowReturnBook> BorrowReturnBookList = new ArrayList<BorrowReturnBook>();
		Scanner sc = new Scanner(System.in);
		
		ServiceCallProgram serviceCallProgram = new ServiceCallProgram();
		
		boolean EndProgram = false;
		//���ҧ User ��� Booklis ����� 1 �ش
		User = createUserTest(User);
		BookList = createBookListTest(BookList);
		
		while (EndProgram == false) {
			System.out.println("\n              <<Select Program>>");
			System.out.println("\nUser Management System              press : 1");
			System.out.println("Book Management System              press : 2");
			System.out.println("BorrowReturnBook Management System  press : 3");
			System.out.println("End Program                         press : 4");
			System.out.print("\nSelect Program : ");
			String Program = sc.next();
			
			switch (Program) {
			case "1":
				serviceCallProgram.CallUserManagement(User);;
				break;
			case "2":
				serviceCallProgram.CallBookManagement(BookList);
				break;
			case "3":
				serviceCallProgram.CallBorrowAndReturnBookManagement(BookList , User , BorrowReturnBookList);
				break;
			case "4":
				System.out.println("\n                   <<<End Program>>>");
				EndProgram = true;
				break;
			default:
				System.out.println("              <<<No number Program>>>");
				break;
			}
		}
		
	}
	
	private static ArrayList<Book> createBookListTest(ArrayList<Book> BookList){
		
		BookTest(BookList , "K" , 1);
		BookTest(BookList , "L" , 1);
		BookTest(BookList , "L" , 1);
		BookTest(BookList , "K" , 2);
		BookTest(BookList , "OOP" , 2);
		BookTest(BookList , "Java" , 5);
		return BookList;
	}
	
	private static ArrayList<Book> BookTest(ArrayList<Book> BookList, String BookName , int Amount) {
		Book book = new Book();
		if(BookList.size() == 0) {
			book.setId(CreateUserId(2));
			book.setName(BookName);
			book.setAmount(Amount);
			book.setAmountBorrow(0);
			book.setAmountNow(book.getAmount()-book.getAmountBorrow());
			BookList.add(book);
		}
		else {
			boolean HasName = false;
			
			book.setName(BookName);
			for(int i =0 ; i<BookList.size();i++) {
				if(BookList.get(i).getName().equals(BookName)) {
					int num = BookList.get(i).getAmount()+Amount;
					book.setId(BookList.get(i).getId());
					book.setAmount(num);
					book.setAmountBorrow(BookList.get(i).getAmountBorrow());
					book.setAmountNow(book.getAmount()-book.getAmountBorrow());
					BookList.set(i ,book);
					HasName = true;
				}
			}
			if(HasName == false) {
				book.setId(CreateUserId(Integer.parseInt(BookList.get(BookList.size()-1).getId()) +2));
				book.setAmount(Amount);
				book.setAmountBorrow(0);
				book.setAmountNow(book.getAmount()-book.getAmountBorrow());
				BookList.add(book);
			}
		}
		
		return BookList;
	}
	
	private static ArrayList<BookUser> createUserTest(ArrayList<BookUser> User) {
		
		User.add(UserTest(User, "Olivia", "student" , "098xxxxxxx"));
		User.add(UserTest(User, "Emma", "student" , "098xxxxxxx"));
		User.add(UserTest(User, "Hannah", "teacher" , "098xxxxxxx"));
		User.add(UserTest(User, "Isabella", "teacher" , "098xxxxxxx"));
		User.add(UserTest(User, "Kittiphum", "staff" , "098xxxxxxx"));
		User.add(UserTest(User, "B", "staff" , "098xxxxxxx"));
		User.add(UserTest(User, "B", "student" , "098xxxxxxx"));
		return User ;
	}
	
	private static BookUser UserTest(ArrayList<BookUser> User, String UserName , String UserType , String PhoneNumber) {
		BookUser bookuser = new BookUser();
		if(User.size() == 0) {
			bookuser.SetUserId(CreateUserId(2));
			bookuser.SetUserName(UserName);
			bookuser.SetUserType(UserType);
			bookuser.SetPhoneNumber(PhoneNumber);
		}else { 
			bookuser.SetUserId(CreateUserId(Integer.parseInt(User.get(User.size()-1).GetUserId()) +2));
			bookuser.SetUserName(UserName);
			bookuser.SetUserType(UserType);
			bookuser.SetPhoneNumber(PhoneNumber);
		}
		return bookuser ;
	}
	
	private static String CreateUserId(int numUser) {
		int lengthID = 5;
		String ID = "";
		for(int i =0 ; i<numUser ;i++) {
			ID = String.valueOf(i);
			if(ID.length()<lengthID) {
				for(int j = 0 ; j<=lengthID-ID.length();j++) {
					ID = "0"+ID ;
				}
			}
		}
		
		return ID;
	}
	
}
