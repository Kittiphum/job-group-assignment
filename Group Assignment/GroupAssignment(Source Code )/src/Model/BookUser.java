package Model;

import InterFace.User;

public class BookUser implements User{
	
	private String UserId;
	
	private String UserName;
	
	private String UserType;
	
	private String PhoneNumber;
	
	public String GetPhoneNumber() {
		return PhoneNumber;
	}

	public String SetPhoneNumber(String PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
		return PhoneNumber;
	}

	@Override
	public String GetUserId() {
		return UserId;
	}

	@Override
	public String SetUserId(String UserId) {
		this.UserId = UserId;
		return UserId;
	}

	@Override
	public String GetUserName() {
		return UserName;
	}

	@Override
	public String SetUserName(String UserName) {
		this.UserName = UserName;
		return UserName;
	}

	@Override
	public String GetUserType() {
		return UserType;
	}

	@Override
	public String SetUserType(String UserType) {
		this.UserType = UserType;
		return UserType;
	}

	

}
