package Model;

import InterFace.Product;

public class Book implements Product{
	
	private String BookId ;
	
	private String BookName;
	
	private int Amount;
	
	private int AmountNow;
	
	private int AmountBorrow;

	@Override
	public String getId() {
		return BookId;
	}

	@Override
	public String setId(String Id) {
		this.BookId = Id;
		return BookId;
	}

	@Override
	public String getName() {
		return BookName;
	}

	@Override
	public String setName(String Name) {
		this.BookName = Name;
		return this.BookName;
	}

	@Override
	public int getAmount() {
		return Amount;
	}

	@Override
	public int setAmount(int Amount) {
		this.Amount = Amount;
		return this.Amount;
	}

	@Override
	public int getAmountNow() {
		return this.AmountNow;
	}

	@Override
	public int setAmountNow(int AmountNow) {
		this.AmountNow = AmountNow;
		return this.AmountNow;
	}

	@Override
	public int getAmountBorrow() {
		return this.AmountBorrow;
	}

	@Override
	public int setAmountBorrow(int AmountBorrow) {
		this.AmountBorrow = AmountBorrow;
		return this.AmountBorrow;
	}

}
