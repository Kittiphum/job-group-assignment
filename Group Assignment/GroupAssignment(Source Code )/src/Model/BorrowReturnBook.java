package Model;

import InterFace.BorrowReturn;

public class BorrowReturnBook implements BorrowReturn{
	
	private String CodeBorrowed;
	
	private String BorrowedDate;
	
	private String WantDateReturn;
	
	private String ReturnDate;
	
	private int AmountBook;
	
	private String UserId;
	
	private String ProductId;
	

	public int getAmountBook() {
		return AmountBook;
	}

	public int setAmountBook(int AmountBook) {
		this.AmountBook = AmountBook;
		return this.AmountBook;
	}

	@Override
	public String getCodeBorrowed() {
		return CodeBorrowed;
	}

	@Override
	public String setCodeBorrowed(String CodeBorrowed) {
		this.CodeBorrowed = CodeBorrowed;
		return this.CodeBorrowed;
	}

	@Override
	public String getBorrowedDate() {
		return BorrowedDate;
	}

	@Override
	public String setBorrowedDate(String BorrowedDate) {
		this.BorrowedDate = BorrowedDate;
		return this.BorrowedDate;
	}

	@Override
	public String getWantDateReturn() {
		return WantDateReturn;
	}

	@Override
	public String setWantDateReturn(String WantDateReturn) {
		this.WantDateReturn = WantDateReturn;
		return this.WantDateReturn;
	}

	@Override
	public String getReturnDate() {
		return ReturnDate;
	}

	@Override
	public String setReturnDate(String ReturnDate) {
		this.ReturnDate= ReturnDate;
		return ReturnDate;
	}

	@Override
	public String getUserId() {
		return UserId;
	}

	@Override
	public String setUserId(String UserId) {
		this.UserId = UserId;
		return UserId;
	}

	@Override
	public String getProductId() {
		return ProductId;
	}

	@Override
	public String setProductId(String ProductId) {
		this.ProductId = ProductId;
		return ProductId;
	}

}
