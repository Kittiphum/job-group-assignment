package service;

import java.util.ArrayList;
import java.util.Scanner;

import Model.Book;
import Model.BookUser;
import Model.BorrowReturnBook;
import Program.BookMangement;
import Program.BorrowReturnBookMangement;
import Program.UserMangement;

public class ServiceCallProgram {
	
	private UserMangement userMangement = new UserMangement();
	private BookMangement bookMangement = new BookMangement();
	private BorrowReturnBookMangement borrowreturnbookmangement = new BorrowReturnBookMangement();
	private serviceUser serviceuser = new serviceUser();
	
	public void CallUserManagement(ArrayList<BookUser> User) {
		
		Scanner sc = new Scanner(System.in);
		
		boolean EndProgram = false;
		
		while (EndProgram == false) {
			System.out.println("\n  <<<User Management System Start>>>");
			System.out.println("\nAdd User                 press : 1");
			System.out.println("Delete User              press : 2");
			System.out.println("Edit User                press : 3");
			System.out.println("Show User                press : 4");
			System.out.println("End Program              press : 5");
			System.out.print("\nSelect Program : ");
			String Program = sc.next();
			
			switch (Program) {
			case "1":
				userMangement.addBookUser(User);
				break;
			case "2":
				userMangement.deleteUser(User);
				break;
			case "3":
				userMangement.editBookUser(User);
				break;
			case "4":
				serviceuser.displayAll(User);
				System.out.println("\n<<<Press Enter to continue>>>");
				try{
					System.in.read();
					}
				catch(Exception e){
					
				}
				break;
			case "5":
				System.out.println("\n              <<End Program>>");
				EndProgram = true;
				break;
			default:
				System.out.println("              <<<No number Program>>>");
				break;
			}
		}
		
	}

	public void CallBookManagement(ArrayList<Book> BookList) {
		
		Scanner sc = new Scanner(System.in);
		
		boolean EndProgram = false;
		
		while (EndProgram == false) {
			System.out.println("\n      <<<Book Management System Start>>>");
			System.out.println("\nAdd Book                 press : 1");
			System.out.println("Delete Book              press : 2");
			System.out.println("Edit Book                press : 3");
			System.out.println("Show Book                press : 4");
			System.out.println("End Program              press : 5");
			System.out.print("\nSelect Program : ");
			String Program = sc.next();
			
			switch (Program) {
			case "1":
				bookMangement.AddBook(BookList);
				break;
			case "2":
				bookMangement.DeleteBook(BookList);
				break;
			case "3":
				bookMangement.EditBook(BookList);
				break;
			case "4":
				System.out.println("\n                       <<< Show Book >>>");
				bookMangement.display(BookList);
				System.out.println("\n<<<Press Enter to continue>>>");
				try{
					System.in.read();
					}
				catch(Exception e){
					
				}
				break;
			case "5":
				System.out.println("\n              <<End Program>>");
				EndProgram = true;
				break;
			default:
				System.out.println("              <<<No number Program>>>");
				break;
			}
		}
		
	}
	
	public void CallBorrowAndReturnBookManagement(ArrayList<Book> BookList , ArrayList<BookUser> User , ArrayList<BorrowReturnBook> BorrowReturnBookList) {
		Scanner sc = new Scanner(System.in);
		boolean EndProgram = false;
		int UserAndBook [] = new int[2];
		int IdBorrowAndBookList[] = new int[2];
		int Amount = 0;
		
		while (EndProgram == false) {
			System.out.println("\n             <<<BorrowReturnBook Management System start>>>");
			System.out.println("\nBorrow Book                             press : 1");
			System.out.println("Return Book                                press : 2");
			System.out.println("Borrow And List Return Book                press : 3");
			System.out.println("End Program                                press : 4");
			System.out.print("\nSelect Program : ");
			String Program = sc.next();
			
			switch (Program) {
			case "1":
				BookMangement bookMangement = new BookMangement();
				
				UserAndBook = borrowreturnbookmangement.NameUserandBookBorrow(BookList, User);
				Amount = borrowreturnbookmangement.UpdateBookList(BookList , UserAndBook);
				borrowreturnbookmangement.UpdateBookList(BookList, UserAndBook, Amount);
				
				borrowreturnbookmangement.addnBorrow(BorrowReturnBookList ,BookList , User, UserAndBook , Amount);
				
				break;
			case "2":
				IdBorrowAndBookList = borrowreturnbookmangement.ReturnBook(BorrowReturnBookList , BookList);
				borrowreturnbookmangement.UpdateReturnBook(BorrowReturnBookList , IdBorrowAndBookList);
				borrowreturnbookmangement.UpdateBorrowReturnBook(BookList , BorrowReturnBookList ,IdBorrowAndBookList);
				System.out.println("\n                       <<< Show Book Borrow List >>>");
				borrowreturnbookmangement.display(BorrowReturnBookList);
				System.out.println("\n<<<Press Enter to continue>>>");
				try{
					System.in.read();
					}
				catch(Exception e){
					
				}
				break;
			case "3":
//				BorrowReturnBook borrow_retur = new BorrowReturnBook();
//				borrow_retur.setCodeBorrowed("0001");
//				borrow_retur.setBorrowedDate("2021-05-10");
//				borrow_retur.setWantDateReturn("2021-05-10");
//				borrow_retur.setReturnDate("Not yet returned");
//				borrow_retur.setUserId("0001");
//				borrow_retur.setProductId("0005");
//				borrow_retur.setAmountBook(10);
//				BorrowReturnBookList.add(borrow_retur);
//				borrow_retur = new BorrowReturnBook();
//				borrow_retur.setCodeBorrowed("0001");
//				borrow_retur.setBorrowedDate("2021-05-10");
//				borrow_retur.setWantDateReturn("2021-05-10");
//				borrow_retur.setReturnDate("Not yet returned");
//				borrow_retur.setUserId("0001");
//				borrow_retur.setProductId("0005");
//				borrow_retur.setAmountBook(1);
				
//				BorrowReturnBookList.add(borrow_retur);
				
				System.out.println("\n                       <<< Show Book Borrow List >>>");
				borrowreturnbookmangement.display(BorrowReturnBookList);
				System.out.println("\n<<<Press Enter to continue>>>");
				try{
					System.in.read();
					}
				catch(Exception e){
					
				}
				break;
			case "4":
				System.out.println("\n              <<End Program>>");
				EndProgram = true;
				break;
			default:
				System.out.println("              <<<No number Program>>>");
				break;
			}
		}
	}
}
