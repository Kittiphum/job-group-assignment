package service;

import java.util.ArrayList;
import java.util.Scanner;

import Model.BookUser;

public class serviceUser {
	
	public BookUser startAdd(BookUser bookuser , int UserId) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("\n<<<Add User Start>>>");
		bookuser.SetUserId(CreateUserId(UserId));
		System.out.print("\nInput Name :");
		String newName = sc.next();
		bookuser.SetUserName(newName);
		
		boolean AddTypecomplete = false;
		String Type[] = {"teacher" , "student","staff"};
		System.out.println();
		while (AddTypecomplete == false) {
			for(int i =0 ; i<Type.length;i++) {
				System.out.println(Type[i] + " press : " + i);
			}
			System.out.print("\nSelect New Type : ");
			int inputType = sc.nextInt();
			if(inputType < Type.length) {
				bookuser.SetUserType(Type[inputType]);
				AddTypecomplete=true;
			}else {
				System.out.println("\n****Incorrect value Select again****");
			}
		}
		
		System.out.print("\nInput Phone :");
		String newPhone = sc.next();
		bookuser.SetPhoneNumber(newPhone);
		
		return bookuser;
	}
	
	public String CreateUserId(int numUser) {
		int lengthID = 5;
		String ID = "";
		for(int i =0 ; i<numUser ;i++) {
			ID = String.valueOf(i);
			if(ID.length()<lengthID) {
				for(int j = 0 ; j<=lengthID-ID.length();j++) {
					ID = "0"+ID ;
				}
			}
		}
		
		return ID;
	}
	
	public ArrayList<BookUser> startEdit(ArrayList<BookUser> UserList , BookUser bookuser,String inputIdEdit) {
		Scanner sc = new Scanner(System.in);
		
		bookuser.SetUserId(inputIdEdit);
		
		System.out.print("\nInput New Name : ");
		String inputNameEdit = sc.next();
		bookuser.SetUserName(inputNameEdit);
		
		boolean EditTypecomplete = false;
		String Type[] = {"teacher" , "student","staff"};
		System.out.println();
		while (EditTypecomplete == false) {
			for(int i =0 ; i<Type.length;i++) {
				System.out.println(Type[i] + " press : " + i);
			}
			System.out.print("\nSelect New Type : ");
			int inputType = sc.nextInt();
			if(inputType < Type.length) {
				bookuser.SetUserType(Type[inputType]);
				EditTypecomplete=true;
			}else {
				System.out.println("\n****Incorrect value Select again****");
			}
		}
		
		System.out.print("\nInput New Phone : ");
		String inputPhoneEdit = sc.next();
		bookuser.SetPhoneNumber(inputPhoneEdit);
		
		UserList.set(Integer.parseInt(inputIdEdit)-1,bookuser);
		
		return UserList;
	}
	
	public void displayIF(ArrayList<BookUser> UserList , String condition) {
		String interval1 = "      ";
		String interval2 = "";//20
		String interval3 = "";//15
		System.out.println("\nUserID   |     UserName       |   Phone      | Type");
		System.out.println("---------------------------------------------------");
		for(int i = 0 ; i < UserList.size() ; i++) {
			if(UserList.get(i).GetUserName().equals(condition)) {
				interval2 = ManageInterval(interval2 , UserList.get(i).GetUserName(), 20);
				interval3 = ManageInterval(interval3 , UserList.get(i).GetPhoneNumber(), 15);
				System.out.println(" "+UserList.get(i).GetUserId()+ interval1 +
									UserList.get(i).GetUserName()+interval2+ 
									UserList.get(i).GetPhoneNumber() +interval3+
									UserList.get(i).GetUserType());
			}
		}
	}
	
	public void displayAll(ArrayList<BookUser> UserList) {
		String interval1 = "      ";
		String interval2 = "";//20
		String interval3 = "";//15
		System.out.println("\nUserID   |     UserName       |   Phone      | Type");
		System.out.println("---------------------------------------------------");
		for(int i = 0 ; i < UserList.size() ; i++) {
			interval2 = ManageInterval(interval2 , UserList.get(i).GetUserName(), 20);
			interval3 = ManageInterval(interval3 , UserList.get(i).GetPhoneNumber(), 15);
			System.out.println(" "+UserList.get(i).GetUserId()+ interval1 +
							UserList.get(i).GetUserName()+interval2+ 
							UserList.get(i).GetPhoneNumber() +interval3+
							UserList.get(i).GetUserType());
			
		}
	}
	
	public String ManageInterval(String interval , String lenName , int intervalMax) {
		interval = "";
		for(int j = 0 ; j<intervalMax-lenName.length() ; j++) {
			interval = " "+interval;
		}
		return interval;
	}

}
