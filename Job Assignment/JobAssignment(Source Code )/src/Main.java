import java.awt.Robot;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		ArrayList<String> TODO = new ArrayList<String>();
		TODO.add("TODO01");
		TODO.add("TODO02");
		TODO.add("TODO03");
		StartTODO(TODO);
		
	 }
	
	private static void StartTODO(ArrayList<String> TODO) {
		Manu menu = new Manu();
		Scanner sc = new Scanner(System.in);
		boolean ExitProgram = false;
		while (ExitProgram==false) {
			System.out.println("\nSelect the desired menu\n");
			System.out.println("AddToDo      press   : "+"1");
			System.out.println("DeleteToDo   press   : "+"2");
			System.out.println("EditToDo     press   : "+"3");
			System.out.println("Show TODO    press   : "+"4");
			System.out.println("ExitProgram  press   : "+"5");
			System.out.print("\nInput number Menu : ");
			String SelectProgram = sc.next();
			
			switch (SelectProgram) {
				case "1":
					System.out.print("\nStart AddToDo");
					TODO = menu.AddToDo(TODO);
					break;
				case "2":
					System.out.print("\nStart DeleteToDo");
					TODO = menu.DeleteToDo(TODO);
					break;
				case "3":
					System.out.print("\nStart EditToDo");
					TODO = menu.EditToDo(TODO);
					break;
				case "4":
					System.out.print("\nGetToDo");
					TODO = menu.GetToDo(TODO , true);
					break;
				case "5":
					System.out.print("\nExit Program");
					ExitProgram=true;
					break;
				default:
					System.out.println("\nThere is no program number ");
					break;
			}
		}
		
	}

	
}
