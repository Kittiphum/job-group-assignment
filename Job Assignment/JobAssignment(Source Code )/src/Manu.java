import java.util.ArrayList;
import java.util.Scanner;

public class Manu {
	
	
	public static ArrayList<String> AddToDo(ArrayList<String> TODO) {
		Scanner sc = new Scanner(System.in);
		boolean HasName = false;
		Object[] TODO_array = TODO.toArray();
		while (HasName == false) {
			
			System.out.print("\nInput TODO Name : ");
			String Input = sc.next();
			for(int i =0; i<TODO_array.length;i++) {
				if(TODO_array[i].equals(Input)) {
					HasName=true;
					break;
				}
			}
			if(HasName==false) {
				TODO.add(Input);
				System.out.println("\nAdd " + Input + " completed");
				HasName=true;
			}else if(HasName==true) {
				System.out.println("\nName is "+ Input + " already have");
				System.out.println("Input Name Again ");
				HasName=false;
			}
			
		}
		System.out.print("\n<<Input anything to exit Program>>");
		String putToexit = sc.next();
		return TODO ;
	}
	
	public static ArrayList<String> DeleteToDo(ArrayList<String> TODO) {
		service Service = new service();
		Scanner sc = new Scanner(System.in);
		boolean DeleteCompleted = false;
		Object[] TODO_array = TODO.toArray();
		GetToDo(TODO, false);
		while (DeleteCompleted == false) {
			System.out.print("\nInput Name TODO want to delete : ");
			String Input = sc.next();
			for(int i =0 ; i<TODO_array.length;i++) {
				if(TODO_array[i].equals(Input)) {
					System.out.println("\nDelete Name "+TODO_array[i] + " Completed");
					TODO_array = Service.remove(TODO_array , i);
					DeleteCompleted = true;
					break;
				}
			}
			if(DeleteCompleted == false) {
				System.out.println("\nDon't have this Name ");
				System.out.print("Name have in TODO right now ");
				GetToDo(TODO , false);
				System.out.println("\nInput Name Again");
			}
		}
		
		TODO = Service.ArrayToList(TODO_array);
		System.out.print("\n<<Input anything to exit Program>>");
		String putToexit = sc.next();
		return TODO ;
	}
	
	public static ArrayList<String> EditToDo(ArrayList<String> TODO) {
		service Service = new service();
		Scanner sc = new Scanner(System.in);
		boolean HasName = false;
		Object[] TODO_array = TODO.toArray();
		
		while (HasName == false) {
			System.out.print("Name have in TODO right now ");
			GetToDo(TODO, false);
			System.out.print("\nInput Name TODO want to Edit : ");
			String Input = sc.next();
			for(int i =0 ; i<TODO_array.length;i++) {
				if(TODO_array[i].equals(Input)) {
					System.out.println("\nHas Name : "+TODO_array[i]);
					System.out.print("Enter name you want to edit : ");
					String Input_EditName = sc.next();
					System.out.print("\nEdit Name "+ TODO_array[i] + "--> ");
					TODO_array[i] = Input_EditName;
					System.out.println(TODO_array[i] + " Completed");
					HasName = true;
					break;
				}
			}
			if(HasName == false) {
				System.out.println("\nDon't have this Name ");
				System.out.println("Input Name Again");
			}
		}
		
		TODO = Service.ArrayToList(TODO_array);
		System.out.print("\n<<Input anything to exit Program>>");
		String putToexit = sc.next();
		return TODO ;
	}
	
	public static ArrayList<String> GetToDo(ArrayList<String> TODO , boolean wait) {
		int idex = 0;
		int newRow = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("\nTotal number of TODO Now = " + TODO.size());
		System.out.println("No.      Name");
		for (String Name : TODO) {
			idex++;
			System.out.println(""+idex + "   |   "+Name);
			
		}
		if(wait == true) {
			System.out.print("\n\n<<Input anything to exit Program>>");
			String putToexit = sc.next();
		}
		return TODO ;
	}

}
