import java.util.ArrayList;

public class service {
	
	public static Object[] remove(Object[] arr, int index){
		
		if (arr == null || index < 0 || index >= arr.length) {
			return arr;
		}
		
		Object anotherArray[] = new Object[arr.length - 1];
		
		for (int i = 0, k = 0; i < arr.length; i++) {
		
			if (i == index) {
				continue;
			}
			
			anotherArray[k++] = arr[i];
		}
		
		return anotherArray;
	}
	
	public static ArrayList<String> ArrayToList(Object [] Array) {
		ArrayList<String> List = new ArrayList<String>();
		for (Object NameTODO : Array) {
			List.add(NameTODO.toString());
		}
		return List;
	}
	
}
